package com.example.thetempsofcelsius;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

public class TempCel extends HttpServlet {
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1> The Fahrenheit temperature is: " +converter.getFahrenheit(request.getParameter("Cel"))+ "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}
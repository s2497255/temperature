package com.example.thetempsofcelsius;

import static java.lang.Integer.parseInt;

public class Converter {
    double getFahrenheit(String Temp){
        double Fahr = parseInt(Temp)*1.8 +32;
        return Fahr;
    }
}
